import { Button } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import './Login.css';

function Login () {
  return (
    <div className="login">
      <Link to="/">
        <img 
          className="login__logo"
          src="http://pngimg.com/uploads/amazon/amazon_PNG11.png"
          alt=""
        />
      </Link>

      <div className="login__container">
        <h1>Sign In</h1>
        <h5>E-mail</h5>
        <input type="text" />
        <h5>Password</h5>
        <input type="text" />
        <Button variant="contained" color="primary">Sign In</Button>
      </div>
    </div>
  )
}

export default Login;