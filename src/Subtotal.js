import React from 'react';
import './Subtotal.css';
import { Button } from '@material-ui/core';
import CurrencyFormat from 'react-currency-format';
import { useStateValue } from './StateProvier';

function Subtotal () {

  const [{basket}, dispatch] = useStateValue();

  return (
    <div className="subtotal">
      <CurrencyFormat

        renderText = {(value) => (
          <>
            <p>
              Subtotal ({basket.length} items): <strong>{value}</strong>
            </p>
            <small className="subtotal__gift">
              <input type="checkbox" /> This order contains a gift.
            </small>
          </>
        )}

        decimalScale={2}
        value={0}
        displayType={"text"}
        thousandSeoarator={true}
        prefix={"$"}
       />
      <Button variant="outlined" color="primary">Proceed to checkout</Button>
    </div>
  )
}

export default Subtotal;