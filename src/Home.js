import React from "react";
import './Home.css';
import Product from './Product';

function Home() {
  return (
    <div className="home">
      <img 
        src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg"
        className="home__banner"
        alt="Home Page Banner" />

      <div className="home__row">
        <Product 
          id="123"
          title="Macbook Air M1"
          price={1100.99}
          rating={5}
          image="https://image.ceneostatic.pl/data/products/99105003/i-apple-macbook-air-13-3-m1-8gb-256gb-macos-mgn63zea.jpg" />
        <Product 
          id="123"
          title="Macbook Air M1"
          price={1100.99}
          rating={5}
          image="https://image.ceneostatic.pl/data/products/99105003/i-apple-macbook-air-13-3-m1-8gb-256gb-macos-mgn63zea.jpg" />
      </div>
      <div className="home__row">
        <Product 
            id="123"
            title="Macbook Air M1"
            price={1100.99}
            rating={5}
            image="https://image.ceneostatic.pl/data/products/99105003/i-apple-macbook-air-13-3-m1-8gb-256gb-macos-mgn63zea.jpg" />
        <Product 
            id="123"
            title="Macbook Air M1"
            price={1100.99}
            rating={5}
            image="https://image.ceneostatic.pl/data/products/99105003/i-apple-macbook-air-13-3-m1-8gb-256gb-macos-mgn63zea.jpg" />
        <Product 
            id="123"
            title="Macbook Air M1"
            price={1100.99}
            rating={5}
            image="https://image.ceneostatic.pl/data/products/99105003/i-apple-macbook-air-13-3-m1-8gb-256gb-macos-mgn63zea.jpg" />
      </div>
      <div className="home__row">
        <Product 
            id="123"
            title="Macbook Air M1"
            price={1100.99}
            rating={5}
            image="https://image.ceneostatic.pl/data/products/99105003/i-apple-macbook-air-13-3-m1-8gb-256gb-macos-mgn63zea.jpg" />
      </div>
    </div>
  );
}

export default Home;
